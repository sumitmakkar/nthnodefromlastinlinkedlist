#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int data;
    node_structure *next;
}node_struct;

class Node
{
    private:
        vector<int>  nodesData;
        node_struct *head;
        node_struct* createNewNode(int num)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data = num;
                newNode->next = NULL;
            }
            return newNode;
        }

    public:
        Node(vector<int> nData)
        {
            head      = NULL;
            nodesData = nData;
        }
    
        node_struct *createLinkedList()
        {
            node_struct *nodePtr  = NULL;
            int          listSize = (int)nodesData.size();
            for(int i = 0 ; i < listSize ; i++)
            {
                node_struct *newNode = createNewNode(nodesData[i]);
                if(!nodePtr)
                {
                    nodePtr = newNode;
                    head    = newNode;
                }
                else
                {
                    nodePtr->next = newNode;
                    nodePtr       = nodePtr->next;
                }
            }
            return head;
        }
    
        void display()
        {
            while(head)
            {
                cout<<head->data<<" ";
                head = head->next;
            }
            cout<<endl;
        }
};

class Engine
{
    private:
        int          nthLastElement;
        node_struct *head;
    
        void printNthLastElementInTheList()
        {
            int          counter  = 0;
            node_struct *frontPtr = head;
            node_struct *backPtr  = head;
            while (frontPtr)
            {
                frontPtr = frontPtr->next;
                if(counter >= nthLastElement)
                {
                    backPtr = backPtr->next;
                }
                counter++;
            }
            cout<<backPtr->data<<endl;
        }
    
    public:
        Engine(node_struct *h , int n)
        {
            head           = h;
            nthLastElement = n;
            printNthLastElementInTheList();
        }
};

int main(int argc, const char * argv[])
{
    vector<int> nodesData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
    Node        n         = Node(nodesData);
    node_struct *listHead = n.createLinkedList();
    Engine(listHead , 5);
    return 0;
}
